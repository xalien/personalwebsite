from  django.shortcuts import render
from  django.http import HttpResponseRedirect
from .models import PersonalInfo
# Create your views here.

def index(request):
    userinfo = PersonalInfo.objects.all();
    return render(request,'index.html',{'userinfo':userinfo})

def home(request):
    return HttpResponseRedirect('/')