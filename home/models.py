from django.db import models
# Create your models here.

class PersonalInfo(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=500)
    photo = models.FileField()

    def __str__(self):
        return self.name