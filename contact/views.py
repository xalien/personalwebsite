from django.shortcuts import render
from home.models import PersonalInfo
# Create your views here.
def contact(request):
    userinfo = PersonalInfo.objects.all();
    return render(request,'contact.html',{'userinfo':userinfo})