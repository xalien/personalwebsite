from django.conf.urls import url
from . import views

app_name = 'blog'

urlpatterns = [
    # /blog/
    url(r'^$', views.blog,name='blog'),
]