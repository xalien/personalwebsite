from django.conf.urls import url
from . import views

app_name = 'personalprofile'

urlpatterns =[
# /profile/
    url(r'^$', views.profile,name='profile'),
]