from django.shortcuts import render
from home.models import PersonalInfo
# Create your views here.
def resume(request):
    userinfo = PersonalInfo.objects.all();
    return render(request,'resume.html',{'userinfo':userinfo})